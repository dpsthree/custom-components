import { Component } from '@angular/core';
import { ToggleAllSelection, TreeNode, TreeNodeSelection } from './list.types';
import { KeyValueEntry } from './key-value-list/key-value-list.component';

const TREE_DATA: TreeNode<void>[] = [
  {
    display: 'Fruit',
    selected: false,
    children: [
      {
        selected: false,
        display: 'Apple',
      },
      {
        selected: false,
        display: 'Banana',
      },
      {
        selected: false,
        display: 'Fruit loops',
      },
    ],
  },
  {
    selected: false,
    display: 'Vegetables',
    children: [
      {
        selected: false,
        display: 'Green',
        children: [
          { selected: false, display: 'Broccoli' },
          { selected: false, display: 'Brussels sprouts' },
        ],
      },
      {
        selected: false,
        display: 'Orange',
        children: [
          { selected: false, display: 'Pumpkins' },
          { selected: false, display: 'Carrots' },
        ],
      },
    ],
  },
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  list: KeyValueEntry[] = [
    { key: 'Content', value: 'Yes' },
    { key: 'Content', value: 'Yes' },
    { key: 'Content', value: 'Yes' },
    { key: 'Really Really Big Key designed to overflow', value: 'Yes' },
    { key: 'Content', value: 'Yes' },
    { key: 'Content', value: 'Yes' },
    { key: 'Content', value: 'Yes' },
    { key: 'Content', value: 'Really Really Big Value designed to overflow' },
    { key: 'Content', value: 'Yes' },
    {
      key: 'Really Really Big Key designed to overflow',
      value: 'Really Really Big Value designed to overflow',
    },
    { key: 'Content', value: 'Yes' },
    { key: 'Content', value: 'Yes' },
    { key: 'Content', value: 'Yes' },
    { key: 'Content', value: 'Yes' },
  ];

  treeList = TREE_DATA;

  processNodeSelection(event: TreeNodeSelection<void>): void {
    console.log('saw node change', event.node, event.nodes);
  }

  processToggleAll(event: ToggleAllSelection<void>): void {
    console.log('seeing toggle all', event.state, event.nodes);
  }
}
