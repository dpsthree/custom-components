import { NestedTreeControl } from '@angular/cdk/tree';
import { EventEmitter, TemplateRef } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { cloneDeep, every, some } from 'lodash';
import { TreeNode, TreeNodeSelection, ToggleAllSelection } from './list.types';
import {
  changeAllChildren,
  findOriginalNode,
  processNodeState,
} from './list-utils';

// Generic class implementation that can be used
// for a component that seeks to drive a tree
// controll. A component should extends this class
// and add the inputs and outputs to the metadata
// of the implementing component.
export class TreeListController<T> {
  canSelect = true;
  categoryLayout: TemplateRef<any> | undefined;
  nodeLayout: TemplateRef<any> | undefined;
  set nodes(nodes: TreeNode<T>[]) {
    if (nodes) {
      this._nodes = cloneDeep(nodes);
      this.dataSource.data = this._nodes;
      this.processTreeState();
    }
  }

  // The control will visually update in response to the event
  // and even manage state for all checked and indeterminate groups.
  // But it is up to the consumer to manage/persist the state of the data
  // If the selection should be rejected, the consumer should pass
  // a new tree array to reset the state

  nodeSelected = new EventEmitter<TreeNodeSelection<T>>();
  toggleAll = new EventEmitter<ToggleAllSelection<T>>();

  allSelected = false;
  someSelected = false;
  // tslint:disable-next-line: variable-name
  private _nodes: TreeNode<T>[] = [];

  treeControl = new NestedTreeControl<TreeNode<T>>(
    (node) => node && node.children
  );
  dataSource = new MatTreeNestedDataSource<TreeNode<T>>();

  constructor() {
    this.dataSource.data = this._nodes;
  }

  selectNode(node: TreeNode<T>): void {
    const origNode = findOriginalNode(node, this._nodes);
    if (origNode) {
      origNode.selected = !origNode.selected;
    }
    this.processTreeState();

    this.nodeSelected.emit({ node, nodes: this._nodes });
  }

  processTreeState(): void {
    this._nodes.forEach((node) => processNodeState(node));
    this.someSelected = some(this._nodes, (n) => n.someSelected);
    this.allSelected = every(this._nodes, (n) => n.selected);
  }

  changeAll(): void {
    this.allSelected = !this.allSelected;
    this.someSelected = this.allSelected;
    changeAllChildren(this.allSelected, this._nodes);
    this.toggleAll.emit({ state: this.allSelected, nodes: this._nodes });
  }

  changeTree(node: TreeNode<T>): void {
    node.selected = !node.selected;
    node.someSelected = node.selected;
    changeAllChildren(node.selected, node.children);
    this.processTreeState();
    this.nodeSelected.emit({ node, nodes: this._nodes });
  }
}
