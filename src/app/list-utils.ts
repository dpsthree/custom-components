import { isEqual, some, every } from 'lodash';
import { TreeNode } from './list.types';

export function findOriginalNode<T>(
  node: TreeNode<T>,
  nodes: TreeNode<T>[] | undefined
): undefined | TreeNode<T> {
  if (nodes) {
    const found = nodes.find((n) => isEqual(n, node));
    if (found) {
      return found;
    } else {
      const processChildren = nodes.map((n) =>
        findOriginalNode(node, n.children)
      );
      return processChildren.find((n) => isEqual(n, node));
    }
  } else {
    return undefined;
  }
}

export function processNodeState<T>(node: TreeNode<T>): void {
  // Handle easy case of leaf node first
  if (!node.children) {
    node.someSelected = node.selected;
  } else {
    node.children.forEach((n) => processNodeState(n));
    // children are now complete
    // sum them up to make determination about self
    node.someSelected = some(node.children, (n) => n.someSelected);
    node.selected = every(node.children, (n) => n.selected);
  }
}

export function changeAllChildren<T>(selected: boolean, nodes: TreeNode<T>[]): void {
  nodes.forEach((n) => {
    n.selected = selected;
    n.someSelected = selected;
    if (n.children) {
      changeAllChildren(selected, n.children);
    }
  });
}
