import { Component, Input } from '@angular/core';
import { TreeListController } from '../tree-list-controller';

@Component({
  selector: 'app-expandable-list',
  templateUrl: './expandable-list.component.html',
  // Work around for broken decorator inheritance
  // tslint:disable-next-line: no-inputs-metadata-property
  inputs: ['canSelect', 'nodes', 'nodeLayout', 'categoryLayout'],
  // tslint:disable-next-line: no-outputs-metadata-property
  outputs: ['nodeSelected', 'toggleAll'],
})
export class ExpandableListComponent<T> extends TreeListController<T> {
  @Input('title') title = '';
}
