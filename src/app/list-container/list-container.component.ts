// Framing component that provides common layout pattern
// for multiple types of list-like layouts.

// See corresponding SCSS for explanations of what aspects
// of appearance are supplied.

import { Component } from '@angular/core';

@Component({
  selector: 'app-list-container',
  templateUrl: './list-container.component.html'
})
export class ListContainerComponent {}
