import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatTreeModule } from '@angular/material/tree';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRippleModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';

import { AppComponent } from './app.component';
import { ListContainerComponent } from './list-container/list-container.component';
import { KeyValueListComponent } from './key-value-list/key-value-list.component';
import { ExpandableListComponent } from './expandable-list/expandable-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarListComponent } from './sidebar-list/sidebar-list.component';
import { SidebarSorterComponent } from './sidebar-sorter/sidebar-sorter.component';
import { SidebarSearchComponent } from './sidebar-search/sidebar-search.component';
import { TreeListComponent } from './tree-list/tree-list.component';

@NgModule({
  declarations: [
    AppComponent,
    ListContainerComponent,
    KeyValueListComponent,
    ExpandableListComponent,
    SidebarListComponent,
    SidebarSorterComponent,
    SidebarSearchComponent,
    TreeListComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatTreeModule,
    MatButtonModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatRippleModule,
    MatSelectModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
