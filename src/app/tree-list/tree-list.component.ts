import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { TreeNode } from '../list.types';

@Component({
  selector: 'app-tree-list',
  templateUrl: './tree-list.component.html',
})
export class TreeListComponent<T> {
  @Input('dataSource') dataSource: MatTreeNestedDataSource<T> | undefined;
  @Input('treeControl') treeControl: NestedTreeControl<TreeNode<T>> | undefined;
  @Input('canSelect') canSelect = false;
  @Input('nodeLayout') nodeLayout: TemplateRef<any> | undefined;
  @Input('categoryLayout') categoryLayout: TemplateRef<any> | undefined;
  // Only helpful if the control can use
  // selection as a visibility indicator
  @Input('visibilityMask') visibilityMask = false;

  @Output('changeTree') changeTree = new EventEmitter<TreeNode<T>>();
  @Output('selectNode') selectNode = new EventEmitter<TreeNode<T>>();

  hasChild = (_: number, node: TreeNode<T>) =>
    !!node.children && node.children.length > 0;
}
