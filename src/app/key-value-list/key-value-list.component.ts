// A specific implementation of a list layout
// This is designed to show key value information pairs
// using the shared appearance of other list-like elements

// See the design specifications for example scenarios
import { Component, Input } from '@angular/core';

export interface KeyValueEntry {
  key: string;
  value: string;
}

@Component({
  selector: 'app-key-value-list',
  templateUrl: './key-value-list.component.html'
})
export class KeyValueListComponent {

  @Input('title') title = '';
  @Input('keyLabel') keyLabel = '';
  @Input('valueLabel') valueLabel = '';
  @Input('list') list: KeyValueEntry[] = [];
}
