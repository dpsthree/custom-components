import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarSorterComponent } from './sidebar-sorter.component';

describe('SideBarSorterComponent', () => {
  let component: SidebarSorterComponent;
  let fixture: ComponentFixture<SidebarSorterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarSorterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarSorterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
