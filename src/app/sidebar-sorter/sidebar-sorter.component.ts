import { Component } from '@angular/core';

enum sortTypes {
  priority = 'Priority',
  alphabetical = 'Alphabetical',
}

@Component({
  selector: 'app-sidebar-sorter',
  templateUrl: './sidebar-sorter.component.html'
})
export class SidebarSorterComponent {
  sortTypes = sortTypes;
  selectedSortType = sortTypes.priority;
}
