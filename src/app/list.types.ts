export interface TreeNode<T> {
  display: string;
  // If a consumer would like to attach additional
  // information to the node they can use this property
  // This is helpful if a user wants to react to a
  // node selection but doesn't want to look-up the
  // original entry in their own list
  genric?: T;
  selected?: boolean;
  someSelected?: boolean;
  children?: TreeNode<T>[];
}

export interface TreeNodeSelection<T> {
  node: TreeNode<T>;
  nodes: TreeNode<T>[];
}

export interface ToggleAllSelection<T> {
  state: boolean;
  nodes: TreeNode<T>[];
}
